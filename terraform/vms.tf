resource "google_compute_instance" "elk-jtunowski" {
  name         = "elk-jtunowski"
  machine_type = "e2-small"
  zone         = "europe-west1-b"

  allow_stopping_for_update = false

  boot_disk {
    auto_delete = false
    initialize_params {
      size = "50"
      type = "pd-balanced"
    }
  }

  service_account {
    email = google_service_account.sa-jtunowski.email
    scopes = [
      "cloud-platform"
    ]
  }

  metadata = {
    "startup-script-url" = "gs://xxx/bootstrap.sh",
  }

  network_interface {
    subnetwork = google_compute_subnetwork.jtunowski-subnetwork.self_link
  }
}