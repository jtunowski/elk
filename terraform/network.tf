resource "google_compute_subnetwork" "jtunowski-subnetwork" {
  name                     = "jenkins-desktop"
  ip_cidr_range            = "10.110.80.0/24"
  network                  = google_compute_network.jtunowski-network.self_link
  region                   = "europe-west1"
  private_ip_google_access = true
}

resource "google_compute_network" "jtunowski-network" {
  name                    = "jtunowski"
  auto_create_subnetworks = "false"
  routing_mode            = "GLOBAL"
}