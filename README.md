# ELK setup by puppet

## Hosts

Created in local environment. Used Oracle VM to create machines. Why Centos 7?
I'm working on this system in daily basis and EOL to - June 30, 2024.


| OS       | Name / IP                             | Description                                |
|----------|---------------------------------------|--------------------------------------------|
| Centos 7 | `puppet.jtunowski` `192.168.1.112`    | Puppet master
| Centos 7 | `elk.jtunowski`       `192.168.1.111` | ELK stack
| MacOS    | personal laptop                       | development

## About stack:

ELK stack was installed by puppet. Modules like puppet-server and puppet-agent was installed by hand. 
Connecting puppet-agent could be automated in startup script in terraform.
Logs are collected from puppet. The idea was to collect logs in json format to easily plug into the ELK Stack using Filebeat.
The assumption was also to easily parse the logs in logstash. 
To achieve that I edited two files: ` etc/puppetlabs/puppetserver/request-logging.xml` & `/etc/filebeat/filebeat.yml`)
For the purposes of the project I disabled the firewall.
I used the cronjob for refreshing the state of `production` branch in puppet server. 

## Infrastructure as code

I also added terraform files (they were not used during the task)


## POC 
Test from puppet node:
```
[root@puppet nodes]# curl http://192.168.1.111:9200/_cat/indices?v
health status index                             uuid                   pri rep docs.count docs.deleted store.size pri.store.size
green  open   .geoip_databases                  enXPLDWjSEiS2_A_LS2Nuw   1   0         41            0     38.8mb         38.8mb
green  open   .apm-custom-link                  XqRSsitoQaC_mpbdNhh9rA   1   0          0            0       226b           226b
green  open   .kibana_task_manager_7.17.6_001   ITwreGemRYOLgtNHOUWJmg   1   0         17         1332      1.1mb          1.1mb
yellow open   filebeat-7.17.6-2022.08.30-000001 dItXp5XXS92ypWuwiXdsfg   1   1        306            0    296.8kb        296.8kb
green  open   .apm-agent-configuration          1c1oItBaQiCuWYhwIOyLTA   1   0          0            0       226b           226b
green  open   .async-search                     rBytQh0MTTawUb_xlIXv3A   1   0          2            0      3.1mb          3.1mb
green  open   .kibana_7.17.6_001                n_aPEHHjSJC57WcTpgXCCg   1   0        407          447      4.9mb          4.9mb
green  open   .tasks                            -f9EoNgaSvW3fDpqLl1rJQ   1   0         38            0     46.7kb         46.7kb

[root@puppet nodes]# curl http://192.168.1.111:9200/_cluster/health?pretty
{
  "cluster_name" : "elk",
  "status" : "yellow",
  "timed_out" : false,
  "number_of_nodes" : 1,
  "number_of_data_nodes" : 1,
  "active_primary_shards" : 11,
  "active_shards" : 11,
  "relocating_shards" : 0,
  "initializing_shards" : 0,
  "unassigned_shards" : 1,
  "delayed_unassigned_shards" : 0,
  "number_of_pending_tasks" : 0,
  "number_of_in_flight_fetch" : 0,
  "task_max_waiting_in_queue_millis" : 0,
  "active_shards_percent_as_number" : 91.66666666666666
}

```
## Screens:

![Alt text](logs.png "Logs in kibana")

![Alt text](dashboard.png "Dashboard in kibana")

## What else?:
We can consider:
- using .gitlab.ci to runs linting tasks on the Puppet code.
- use Grafana (elasticsearch as a source).
- set up a cluster for high availability.
- set up [elasticsearch exporter](https://github.com/prometheus-community/elasticsearch_exporter) to have logs in prometheus format.

